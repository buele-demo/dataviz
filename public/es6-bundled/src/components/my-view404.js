define(["./vargia-app.js"],function(_vargiaApp){"use strict";class MyView404 extends _vargiaApp.PageViewElement{static get styles(){return[_vargiaApp.SharedStyles]}render(){return _vargiaApp.html`
      <section>
        <h2>Oops! You hit a 404</h2>
        <p>
          The page you're looking for doesn't seem to exist. Head back
          <a href="/">home</a> and try again?
        </p>
      </section>
    `}}window.customElements.define("my-view404",MyView404)});